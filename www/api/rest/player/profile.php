<?php
/**
 * Generate user profile information at
 * GET http://l2.gamestickservices.net/api/rest/player/profile/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/AchievementRepository.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GamestickRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$achievementRepo = new AchievementRepository();


$nowMilli = time() * 1000;
$data = [
    'sid'             => $gamestick->sessionId,
    'time'            => (string) $nowMilli,
    'lastaccessed'    => $nowMilli,
    'created'         => $nowMilli,
    'accessCount'     => 0,
    'x-forwarded-for' => null,
    'addr'            => '1.2.3.4',
    'remoteaddr'      => '1.2.3.4',

    'body' => [
        'success' => true,
        'action'  => null,
        'message' => null,

        'accountType'    => 'CONSUMER',
        'gamertag'       => $user->gamerTag,
        'avatarLargeUrl' => $user->getAvatarLargeUrl(),
        'avatarSmallUrl' => $user->getAvatarSmallUrl(),
        'minAge'         => $user->minAge,
        'minAgeLabel'    => $user->minAge . '+',

        'founderFlag'    => (int) $user->founderFlag,
        'founderName'    => (string) $user->founderName,

        'password'       => '81dc9bdb52d04dc20036dbd8313ed055',

        'location'       => 'GB',
        'currency'       => 'GBP',
        'dateOfBirth'    => '23/01/1970',
        'dateJoined'     => date('d/m/Y', strtotime($user->created_at)),
        'email'          => 'dummy@example.org',

        'achievementStatus' => [
            'lastAchievementGameName' => $achievementRepo->getLatestAchievementGameName(
                $user->id
            ),
            'numberOfAchievementsUnlocked' => $achievementRepo->getTotalUserAchievements(
                $user->id
            ),
        ],

        'balance' => [
            'amountOfMoneyLeft' => 'GBP 23.42',
            'transactions' => [
            ],
        ],

        'securityLevel' => 1,
    ],
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
