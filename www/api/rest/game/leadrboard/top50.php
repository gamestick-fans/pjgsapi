<?php
/**
 * Get the top 50 scores from the game's leaderboard
 *
 * GET /api/rest/game/xxx/leadrboard/top50/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

$rootDir = dirname(__FILE__, 6);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GameInfo.php';
require_once $rootDir . '/src/GamestickRepository.php';
require_once $rootDir . '/src/LeaderboardRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$gameInfo = new GameInfo();
$gameId = $gameInfo->getIdFromUuid($gameuuid);
ApiResponses::checkGameId($gameId);

$leaderRepo = new LeaderboardRepository();
$top50 = $leaderRepo->getTop50($gameId);

$leaderBoardData = [];
$pos = 1;
foreach ($top50 as $scoreRow) {
    $leaderBoardData[] = [
        'position'    => $pos,
        'score'       => $scoreRow->score,
        'userPlayTag' => $scoreRow->gamerTag,
        'avatarId'    => 11,//???
    ];
    $pos++;
}

$data = [
    'body' => [
        'success' => true,
        'leaderBoard' => $leaderBoardData,
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
