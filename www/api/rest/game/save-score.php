<?php
/**
 * Save a score on the leaderboard
 *
 * GET /api/rest/game/xxx/save-score/4627/extend/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

if (!isset($_GET['score']) || $_GET['score'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Score missing\n";
    exit(1);
}
$score = $_GET['score'];

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GameInfo.php';
require_once $rootDir . '/src/GamestickRepository.php';
require_once $rootDir . '/src/LeaderboardRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$gameInfo = new GameInfo();
$gameId = $gameInfo->getIdFromUuid($gameuuid);
ApiResponses::checkGameId($gameId);

$leaderRepo = new LeaderboardRepository();
$leaderRepo->createOrUpdate($user->id, $gameId, $score);


header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo '{"body":{"success":true}}' . "\n";
