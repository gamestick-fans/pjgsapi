<?php
/**
 * Load the game state so players can continue their game
 *
 * GET /api/rest/game/xxx/state/view.json;jsessionid=zzz
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GameInfo.php';
require_once $rootDir . '/src/GamestickRepository.php';
require_once $rootDir . '/src/SaveRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$gameInfo = new GameInfo();
$gameId = $gameInfo->getIdFromUuid($gameuuid);
ApiResponses::checkGameId($gameId);

$saveRepo = new SaveRepository();
$data = $saveRepo->get($user->id, $gameId);

if ($data === null) {
    header('HTTP/1.0 404 Not Found');
    header('Content-Type: text/plain');
    echo "No saved game state\n";
    exit(1);
}

$data = [
    'body' => [
        'success'   => true,
        'gameState' => base64_encode($data),
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
