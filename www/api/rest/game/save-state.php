<?php
/**
 * Save the game state so players can continue later
 *
 * GET /api/rest/game/xxx/save-state/view.json;jsessionid=zzz?data=MTA%3D
 *
 * Data are base64 encoded
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

if (!isset($_GET['data'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Data missing\n";
    exit(1);
}
$data = base64_decode($_GET['data']);
if ($data === false) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Data not base64 enoded\n";
    exit(1);
}

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GameInfo.php';
require_once $rootDir . '/src/GamestickRepository.php';
require_once $rootDir . '/src/SaveRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$gameInfo = new GameInfo();
$gameId = $gameInfo->getIdFromUuid($gameuuid);
ApiResponses::checkGameId($gameId);

$saveRepo = new SaveRepository();
$saveRepo->createOrUpdate($user->id, $gameId, $data);


header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo '{"body":{"success":true}}' . "\n";
