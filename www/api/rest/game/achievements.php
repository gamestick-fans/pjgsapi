<?php
/**
 * List of available achievements for a game
 * Profile > Achievements > select a game
 *
 * GET http://l2.gamestickservices.net/api/rest/game/xxx/achievements/view.json
 *
 * Image aspect ratio is 7:4 (1.75:1), and somewhere around 177x101.
 * The fallback images are 118x118 and 177x177 in
 * /data/GameStickCache/Assets/textures/achievements/medal_placeholder.png
 * but they get scaled badly.
 *
 * Resizing images:
 * convert achievement_10.png -resize 177x101 -background transparent -gravity center -extent 177x101 sized_achievement_10.png
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/AchievementRepository.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GamestickRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$achievementRepo = new AchievementRepository();

$achievements = $achievementRepo->getCachedAchievements();
$achievementsByGame = [];
$gameId = null;
foreach ($achievements as $achievement) {
    $achievementsByGame[$achievement->gameId][] = $achievement;
    if ($gameId === null && $gameuuid === $achievement->gameUuid) {
        $gameId = $achievement->gameId;
    }
}

if ($gameId === null) {
    //FIXME: better return empty array?
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game not found for UUID (or game has no known achievements)\n";
    exit(1);
}

$userAchievements = $achievementRepo->getUserAchievements($user->id, $gameId);

$achievementData = [];
foreach ($achievementsByGame[$gameId] as $achievement) {
    $achievementData[] = [
        "id"              => $achievement->id,
        "achievementType" => null,
        "achievementName" => $achievement->title,
        "description"     => $achievement->description,
        "xpValue"         => 1,
        "fileName"        => null,
        "fileUrl"         => $achievement->image ?? null,
        "isCurrentUserOwner" => (int) isset($userAchievements[$achievement->id]),
    ];
}

//show obtained achievements first
usort(
    $achievementData,
    function ($infoA, $infoB) {
        return $infoB['isCurrentUserOwner'] - $infoA['isCurrentUserOwner'];
    }
);

$data = [
    'body' => [
        'success' => true,
        'gameId' => $gameuuid,
        'gameList' => $achievementData,
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
