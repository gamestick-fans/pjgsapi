<?php
/**
 * Generate the apps list + other information available at
 * http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 4);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';

$cacheDir = $rootDir . '/cache/';

$nowMilli = time() * 1000;

if (!isset($_GET['hwid'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Hardware ID is missing\n";
    exit(1);
}
$hwId = strtolower($_GET['hwid']);

ApiResponses::checkWhitelist($hwId);

$sessionId = null;
if (isset($_GET['jsessionid'])) {
    $sessionId = $_GET['jsessionid'];
} else if (isset($_COOKIE['JSESSIONID'])) {
    $sessionId = $_COOKIE['JSESSIONID'];
}

require $rootDir . '/src/GamestickRepository.php';
$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findByHardwareId($hwId);

if ($gamestick === null || !$gamestick->getUser()->complete()) {
    //unregistered gamestick
    if ($gamestick === null) {
        $gamestick = $gamestickRepo->create($hwId);
    }

    $data = [
        'sid'  => $gamestick->sessionId,
        'time' => (string) $nowMilli,
        'body' => [
            'status'           => 'CONNECTION_IN_PROGRESS',
            'registrationCode' => ($GLOBALS['verificationCodePrefix'] ?? '')
                . $gamestick->verificationCode,
        ]
    ];
    $json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    echo $json . "\n";
    exit(0);
}


$data = [
    'sid'             => $gamestick->sessionId,
    'time'            => (string) $nowMilli,
    'lastaccessed'    => $nowMilli,
    'x-forwarded-for' => null,
    'created'         => $nowMilli,
    'accessCount'     => 1,
    'addr'            => '1.2.3.4',
    'remoteaddr'      => '1.2.3.4',

    'body' => [
        'status' => 'CONNECTED',
        'verificationCode' => $gamestick->verificationCode,
        'config' => [
            'apps'   => 'PLACEHOLDER_APPS',
            'global' => [
                'defaults' => [
                    'images'   => [],
                    'currency' => null,
                    'social'   => [],
                ],
                'uitranslation' => 'PLACEHOLDER_UITRANSLATION',
                'newfeatured' => [
                    'ages' => 'PLACEHOLDER_AGES',
                ],
            ],
        ]
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


$placeholderFiles = [
    'PLACEHOLDER_APPS'          => $cacheDir . 'connect-apps.min.json',
    'PLACEHOLDER_AGES'          => $cacheDir . 'connect-featured-ages.min.json',
    'PLACEHOLDER_UITRANSLATION' => $cacheDir . 'connect-uitranslation.min.json',
];
foreach ($placeholderFiles as $placeholder => $cacheFile) {
    //inject apps
    if (!file_exists($cacheFile)) {
        header('Content-Type: text/plain');
        echo "Cache file missing: $cacheFile\n";
        exit(1);
    }
    $json = str_replace(
        '"' . $placeholder . '"',
        trim(file_get_contents($cacheFile)),
        $json
    );
}

if ($GLOBALS['toofast']) {
    sleep(1);//prevent error "Failed to duplicate connect data for console"
}

header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
?>
