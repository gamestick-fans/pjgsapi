<?php
/**
 * The user has unlocked an in-game achievement
 *
 * GET http://l2.gamestickservices.net/api/rest/user/achievement/461/set-complete/view.json;jsessionid=zzz
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/AchievementRepository.php';
require_once $rootDir . '/src/GamestickRepository.php';

if (!isset($_GET['jsessionid'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Session ID missing\n";
    exit(1);
}
$sessionId = $_GET['jsessionid'];

if (!isset($_GET['achievementid']) || $_GET['achievementid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "achievement id missing\n";
    exit(1);
}
$achievementId = $_GET['achievementid'];

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
if ($gamestick === null) {
    header('HTTP/1.0 404 Not Found');
    header('Content-Type: text/plain');
    echo "Unknown session ID\n";
    exit(1);
}

$achievementRepo = new AchievementRepository();
$achievementRepo->createOrUpdate($gamestick->userId, $achievementId);


header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo '{"body":{"success":true}}' . "\n";
?>
