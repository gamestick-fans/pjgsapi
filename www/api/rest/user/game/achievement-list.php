<?php
/**
 * List all achievements for the game and if the user has it
 * GET http://l2.gamestickservices.net/api/rest/user/game/xxx/achievement/list/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

if (!isset($_GET['gameuuid']) || $_GET['gameuuid'] === '') {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game UUID missing\n";
    exit(1);
}
$gameuuid = $_GET['gameuuid'];

$rootDir = dirname(__FILE__, 6);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/AchievementRepository.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GamestickRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$achievementRepo = new AchievementRepository();

$achievements = $achievementRepo->getCachedAchievements();
$achievementsByGame = [];
$gameId = null;
foreach ($achievements as $achievement) {
    $achievementsByGame[$achievement->gameId][] = $achievement;
    if ($gameId === null && $gameuuid === $achievement->gameUuid) {
        $gameId = $achievement->gameId;
    }
}

if ($gameId === null) {
    //FIXME: better return empty array?
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Game not found for UUID (or game has no known achievements)\n";
    exit(1);
}

$userAchievements = $achievementRepo->getUserAchievements($user->id, $gameId);

$achievementData = [];
foreach ($achievementsByGame[$gameId] as $achievement) {
    $achievementData[] = [
        "id"              => $achievement->id,
        "achievementType" => null,
        "achievementName" => $achievement->title,
        "description"     => $achievement->description,
        "xpValue"         => 1,
        "fileName"        => null,
        "fileUrl"         => null,
        "isCurrentUserOwner" => (int) isset($userAchievements[$achievement->id]),
    ];
}

if ($gameuuid === '0a673ef9fe281fc4849897e1bb6ebf7c') {
    //Boulder dash wants achievements directly in the body
    $data = [
        'body' => $achievementData,
    ];

} else {
    //Bloo Kid and console#com.playjam.gamestick.databaseinterfaceservice.Achievements
    // want it as property
    $data = [
        'body' => [
            'success' => true,
            'achievements' => $achievementData,
        ],
    ];
}

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
?>
