<?php
/**
 * Achievement summary shown when visiting the profile on the gamestick
 * GET http://l2.gamestickservices.net/api/rest/user/achievement/summary/view.json
 *
 * @see com.playjam.DatabaseService.apk#com.playjam.Services.Database.AchievementSummaryData
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 6);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/AchievementRepository.php';
require_once $rootDir . '/src/ApiResponses.php';
require_once $rootDir . '/src/GamestickRepository.php';

$sessionId = ApiResponses::checkAndGetSessionId();

$gamestickRepo = new GamestickRepository();
$gamestick = $gamestickRepo->findBySessionId($sessionId);
ApiResponses::checkGamestick($gamestick);
ApiResponses::checkWhitelist($gamestick->hwId);

$user = $gamestick->getUser();
ApiResponses::checkUserComplete($user);

$achievementRepo = new AchievementRepository();
$gamesSummary = $achievementRepo->getSummary($user->id);

$achievements = $achievementRepo->getCachedAchievements();
$achievementsByGame = [];
foreach ($achievements as $achievement) {
    $achievementsByGame[$achievement->gameId][] = $achievement;
}

$summaryData = [];
foreach ($gamesSummary as $gameSummary) {
    $achievementId = $gameSummary->latest->achievementId;

    $summaryData[] = (object) [
        'gameId'   => $gameSummary->latest->gameId,
        'gameName' => $achievements[$achievementId]->gameName ?? 'Unknown',
        //the typo in the property is required
        'gameIdentifcation' => $achievements[$achievementId]->gameUuid ?? null,
        'lastAchievementImageUrl' => null,
        'lastAchievementName'     => $achievements[$achievementId]->title ?? 'Unknown',
        'totalNumberofAchievements' => count(
            $achievementsByGame[$gameSummary->latest->gameId] ?? []
        ),
        'numberOfPlayerUnlockedAchievements' => $gameSummary->total,
    ];
}

usort(
    $summaryData,
    function ($summaryA, $summaryB) {
        return strcmp($summaryA->gameName, $summaryB->gameName);
    }
);

$data = [
    'body' => [
        'success' => true,
        'gameAchievementSummary' => $summaryData
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
