<?php
/**
 * Check if everything is fine
 */
header('HTTP/1.0 500 Internal Server Error');

function error($msg)
{
    header('HTTP/1.0 500 Internal Server Error');
    header('Content-Type: text/plain');
    echo 'Error: ' . $msg . "\n";
    exit(1);
}

$configFile = __DIR__ . '/../config.php';
if (!file_exists($configFile)) {
    error('config.php missing. Copy config.php.dist to config.php');
}

require_once $configFile;
require_once __DIR__ . '/../src/Db.php';

try {
    $db = new DB();
    $db->query('SELECT COUNT(*) as num FROM gamesticks')->fetchColumn();
} catch (\PDOException $e) {
    error('Database connection problem: ' . $e->getMessage());
}

if (isset($GLOBALS['popuplarTxtFile'])
    && strlen($GLOBALS['popuplarTxtFile'])
    && !file_exists($GLOBALS['popuplarTxtFile'])
) {
    error('Popular games file does not exist: ' . $GLOBALS['popuplarTxtFile']);
}

if (isset($GLOBALS['featuredFile'])
    && strlen($GLOBALS['featuredFile'])
    && !file_exists($GLOBALS['featuredFile'])
) {
    error('Featured games file does not exist: ' . $GLOBALS['featuredFile']);
}

$cacheDir = __DIR__ . '/../cache/';
$appsCacheFile         = $cacheDir . 'connect-apps.min.json';
$featuredAgesCacheFile = $cacheDir . 'connect-featured-ages.min.json';

if (!file_exists($appsCacheFile)) {
    error('Apps cache file does not exist. Run bin/generate-apps-cache.php');
}
if (!file_exists($featuredAgesCacheFile)) {
    error('Featured ages cache file does not exist. Run bin/generate-apps-cache.php');
}


$firmwareBaseDir  = __DIR__ . '/firmware-updates/';
foreach (glob($firmwareBaseDir . '/*/') as $firmwareDir) {
    $version = basename($firmwareDir);
    if (!file_exists($firmwareDir . '/target-version.txt')) {
        error('Firmware update ' . $version . ' misses target-version.txt');
    }
    if (!file_exists($firmwareDir . '/filesize')) {
        error('Firmware update ' . $version . ' misses filesize');
    }
    if (!file_exists($firmwareDir . '/numchunks')) {
        error('Firmware update ' . $version . ' misses number of chunks');
    }
}


//FIXME: api tests

header('HTTP/1.0 200 OK');
header('Content-Type: text/plain');
echo "Everything looks fine\n";
?>
