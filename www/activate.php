<?php
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 2);
$tplDir = $rootDir . '/templates';

require_once $rootDir . '/config.php';
require $rootDir . '/src/GamestickRepository.php';
$gamestickRepo = new GamestickRepository();

$error   = null;
$code    = '';
$gamestick = null;
if (isset($_REQUEST['code']) && trim($_REQUEST['code']) !== '') {
    $code = $_REQUEST['code'];

    $gamestick = $gamestickRepo->findByVerificationCode($code);
    if ($gamestick === null) {
        $error = 'Invalid code';
    }

    if ($code === 'success') {
        header('HTTP/1.0 200 OK');
        require $tplDir . '/activate-success.phtml';
        exit();
    }
}

if ($gamestick === null) {
    header('HTTP/1.0 200 OK');
    require $tplDir . '/activate-code.phtml';
    exit();
}

$user = $gamestick->getUser();
$input = [
    'gamerTag'    => $_POST['gamerTag'] ?? $user->gamerTag,
    'founderFlag' => (bool) ($_POST['founderFlag'] ?? $user->founderFlag),
    'founderName' => $_POST['founderName'] ?? $user->founderName,
    'minAge'      => $_POST['minAge'] ?? $user->minAge ?? 3,
    'avatar'      => $_POST['avatar'] ?? $user->avatar ?? 'rocket',
    'submit'      => $_POST['submit'] ?? false,
];

$avatars = [
    $input['avatar'] => null,//have active one first, especially for mobile
];
$avatarFiles = glob(__DIR__ . '/../www/resources/avatars/*.small.{jpg,png}', GLOB_BRACE);
foreach ($avatarFiles as $smallImage) {
    $key = basename($smallImage, '.small.jpg');
    $key = basename($key, '.small.png');
    $avatars[$key] = '/resources/avatars/' . basename($smallImage);
}
$avatars = array_filter($avatars);

//input validation
$errors = [];
if (!preg_match('#^[A-Za-z0-9 -]+$#', $input['gamerTag'])) {
    $errors['gamerTag'] = 'Invalid gamer tag';
}
if ($input['founderFlag']) {
    if ($input['founderName'] === '') {
        $errors['founderName'] = 'Founder name missing';
    } else if (!preg_match('#^[A-Za-z0-9 -]+$#', $input['founderName'])) {
        $errors['founderName'] = 'Invalid founder name';
    }
}
if (!in_array($input['minAge'], [3, 7, 12, 17])) {
    $errors['minAge'] = 'Invalid age';
}
if (!in_array($input['avatar'], array_keys($avatars))) {
    $errors['avatar'] = 'Invalid avatar image';
}

if (!$input['submit'] || count($errors)) {
    header('HTTP/1.0 200 OK');
    require $tplDir . '/activate-profile.phtml';
    exit();
}

//validation successful, store the profile
//$input['verificationCode'] => null;
unset($input['submit']);
$user = $user->update($input);

header('HTTP/1.0 200 OK');
require $tplDir . '/activate-success.phtml';
