<?php
/**
 * Firmware update check
 *
 * Each firmware update need a folder in www/firmware-updates/,
 * e.g. www/firmware/0.9.2049/ is the update from 2049 to the next one, 2058.
 *
 * Each folder needs to contain:
 * - changelog.txt
 * - target-version.txt (only the firmware version "x.y.z")
 * - update.img (firmware update)
 *
 * Changelog is available in the update file, e.g.
 * $ unzip -p GameStick-Software-v2049.img data/GameStickCache/current.info
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 2);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ApiResponses.php';

if (!isset($_POST['v'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "POST data missing\n";
    exit(1);
}

$gsData = json_decode($_POST['v']);
if ($gsData === null) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "POST data invalid\n";
    exit(1);
}

if (!isset($gsData->hwid)
    || !isset($gsData->major)
    || !isset($gsData->minor)
    || !isset($gsData->revision)
    || !isset($gsData->platform)
) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "POST data incomplete\n";
    exit(1);
}

$hwId = strtolower($gsData->hwid);
if (!ApiResponses::isWhitelisted($hwId)) {
    header('HTTP/1.0 200 OK');
    header('Content-type: application/json');
    header('X-Problem: Gamestick not whitelisted');
    echo '{"available":false}' . "\n";
    exit(0);
}

$gsVersion = $gsData->major . '.' . $gsData->minor . '.' . $gsData->revision;

$firmwareDir  = $rootDir . '/www/firmware-updates/' . $gsVersion;
$requireUpdate = is_dir($firmwareDir);
if (!$requireUpdate) {
    header('HTTP/1.0 200 OK');
    header('Content-type: application/json');
    echo '{"available":false}' . "\n";
    exit(0);
}

$firmwareFile = $firmwareDir . '/update.img';
if (!file_exists($firmwareFile)) {
    header('HTTP/1.0 200 OK');
    header('Content-type: application/json');
    header('X-Problem: Firmware file missing');
    echo '{"available":false}' . "\n";
    exit(0);
}

$targetFile = $firmwareDir . '/target-version.txt';
if (!file_exists($targetFile)) {
    header('HTTP/1.0 200 OK');
    header('Content-type: application/json');
    header('X-Problem: target-version.txt file missing');
    echo '{"available":false}' . "\n";
    exit(0);
}

$targetVersion = trim(file_get_contents($targetFile));
list($targetMajor, $targetMinor, $targetRevision) = explode('.', $targetVersion);

$changelogFile = $firmwareDir . '/changelog.txt';
$changelog = '';
if (file_exists($changelogFile)) {
    $changelog = file_get_contents($changelogFile);
}

$data = [
    'available'   => true,
    'major'       => $targetMajor,
    'minor'       => $targetMinor,
    'revision'    => $targetRevision,
    'forced'      => false,
    'name'        => $targetVersion,
    'description' => $changelog,
    'timestamp'   => filemtime($firmwareFile),
    'url'         => 'http://update.gamestickservices.net/firmware-updates/download?from=' . $gsVersion,
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
