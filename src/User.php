<?php
require_once 'UserRepository.php';

/**
 * Database row from the "users" table
 */
class User
{
    protected string $avatarDir;

    public int $id;
    public ?string $gamerTag;
    public bool $founderFlag;
    public ?string $founderName;
    public int $minAge;
    public ?string $avatar;

    public string $created_at;
    public ?string $updated_at;

    public function __construct()
    {
        $this->avatarDir = dirname(__FILE__) . '/../www/resources/avatars/';
    }

    public function complete(): bool
    {
        return $this->gamerTag !== null;
    }

    public function getAvatarLargeUrl(): string
    {
        if (strpos($this->avatar, '://')) {
            return $this->avatar;
        }

        if (file_exists($this->avatarDir . $this->avatar . '.large.jpg')) {
            $ext = 'jpg';
        } else {
            $ext = 'png';
        }

        return 'http://l2.gamestickservices.net/resources/avatars/'
            . $this->avatar . '.large.' . $ext;
    }

    public function getAvatarSmallUrl(): string
    {
        if (strpos($this->avatar, '://')) {
            return $this->avatar;
        }

        if (file_exists($this->avatarDir . $this->avatar . '.small.jpg')) {
            $ext = 'jpg';
        } else {
            $ext = 'png';
        }

        return 'http://l2.gamestickservices.net/resources/avatars/'
            . $this->avatar . '.small.' . $ext;
    }

    public function update(array $values): User
    {
        $userRepository = new UserRepository();
        return $userRepository->update($this->id, $values);
    }
}
