<?php

/**
 * Database row from the "achievements" table
 */
class Achievement
{
    public int $id;
    public int $userId;
    public int $achievementId;
    public ?int $gameId = null;

    public string $created_at;
    public string $updated_at;
}
