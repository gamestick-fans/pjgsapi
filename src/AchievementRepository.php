<?php
require_once 'Achievement.php';
require_once 'Db.php';

/**
 * Functions to work with player achievements
 */
class AchievementRepository
{
    protected Db $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function createOrUpdate(int $userId, int $achievementId): void
    {
        $stmt = $this->db->prepare(
            'SELECT id FROM achievements'
            . ' WHERE userId = :userId AND achievementId = :achievementId'
        );
        $stmt->execute([':userId' => $userId, ':achievementId' => $achievementId]);
        $rowId = $stmt->fetchColumn();

        $gameId = $this->getGameIdFromAchievementId($achievementId);

        if ($rowId === false) {
            $stmt = $this->db->prepare(
                <<<SQL
                INSERT INTO achievements
                (userId, achievementId, gameId, created_at, updated_at)
                VALUES (:userId, :achievementId, :gameId, :created_at, :updated_at)
                SQL
            );
            $res = $stmt->execute(
                [
                    ':userId'           => $userId,
                    ':achievementId'    => $achievementId,
                    ':gameId'           => $gameId,
                    ':created_at'       => date('Y-m-d H:i:s'),
                    ':updated_at'       => date('Y-m-d H:i:s'),
                ]
            );

        } else {
            $stmt = $this->db->prepare(
                <<<SQL
                UPDATE achievements SET
                    updated_at = :updated_at,
                    gameId = :gameId
                WHERE id = :id
                SQL
            );
            $stmt->execute(
                [
                    ':id'          => $rowId,
                    ':updated_at'  => date('Y-m-d H:i:s'),
                    ':gameId'      => $gameId,
                ]
            );
        }
    }

    public function getLatestAchievement(int $userId): ?Achievement
    {
        $stmt = $this->db->prepare(
            'SELECT * FROM achievements'
            . ' WHERE userId = :userId'
            . ' ORDER BY updated_at DESC'
            . ' LIMIT 1'
        );
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Achievement');
        $stmt->execute([':userId' => $userId]);
        $row = $stmt->fetch();
        return $row === false ? null : $row;
    }

    public function getLatestAchievementGameName(int $userId): ?string
    {
        $achievement = $this->getLatestAchievement($userId);
        if (!$achievement) {
            return null;
        }

        $achievements = $this->getCachedAchievements();
        return $achievements[$achievement->achievementId]->gameName ?? null;
    }

    public function getTotalUserAchievements(int $userId): int
    {
        $stmt = $this->db->prepare(
            'SELECT COUNT(*) as num FROM achievements'
            . ' WHERE userId = :userId'
        );
        $stmt->execute([':userId' => $userId]);
        return $stmt->fetchColumn();
    }

    /**
     * Latest achievements for each game, and total number of achievements
     * in each game
     */
    public function getSummary(int $userId): array
    {
        $stmt = $this->db->prepare(
            <<<SQL
                SELECT adata.*
                FROM `achievements` AS adata
                JOIN (
                  SELECT MAX(updated_at) AS max_updated_at, gameId
                    FROM achievements
                    WHERE userId = :userId
                    GROUP BY gameId
                ) AS maxa
                WHERE adata.gameId = maxa.gameId
                  AND adata.updated_at = maxa.max_updated_at
            SQL
        );
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Achievement');
        $stmt->execute([':userId' => $userId]);

        $achievements = $stmt->fetchAll();

        $stmt = $this->db->prepare(
            <<<SQL
                SELECT gameId, COUNT(gameId) AS num
                FROM `achievements`
                WHERE userId = :userId
                  AND gameId IS NOT NULL
                GROUP BY gameId
            SQL
        );
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $stmt->execute([':userId' => $userId]);
        $totals = $stmt->fetchAll();
        $totals = array_combine(
            array_column($totals, 'gameId'),
            array_column($totals, 'num')
        );

        $games = [];
        foreach ($achievements as $achievement) {
            $games[] = (object) [
                'total'  => $totals[$achievement->gameId],
                'latest' => $achievement,
            ];
        }
        return $games;
    }

    /**
     * Get all achievements the user has for a given game
     *
     * @return Achievement[] Key is the achievement ID
     */
    public function getUserAchievements(int $userId, int $gameId): array
    {
        $stmt = $this->db->prepare(
            'SELECT * FROM achievements'
            . ' WHERE userId = :userId AND gameId = :gameId'
            . ' ORDER BY achievementId'
        );
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Achievement');
        $stmt->execute([':userId' => $userId, ':gameId' => $gameId]);
        $achievements = $stmt->fetchAll();

        return array_combine(
            array_column($achievements, 'achievementId'),
            $achievements
        );
    }

    protected function getGameIdFromAchievementId(int $achievementId): ?int
    {
        $achievements = $this->getCachedAchievements();
        return $achievements[$achievementId]->gameId ?? null;
    }

    public function getCachedAchievements(): array
    {
        $cacheDir = __DIR__ . '/../cache/';
        $achievementsCacheFile = $cacheDir . 'achievements.min.json';
        return (array) json_decode(file_get_contents($achievementsCacheFile));
    }
}
