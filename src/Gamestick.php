<?php
require_once 'User.php';

/**
 * Database row from the "gamesticks" table
 */
class Gamestick
{
    public int $id;
    public string $hwId;
    public string $sessionId;
    public ?string $verificationCode;
    public ?int $userId = null;

    public string $created_at;
    public string $updated_at;

    public function getUser(): User
    {
        $userRepository = new UserRepository();
        if ($this->userId === null) {
            $user = $userRepository->create();
            $this->update(['userId' => $user->id]);
            $this->userId = $user->id;
        } else {
            $user = $userRepository->findById($this->userId);
        }
        return $user;
    }

    public function update(array $values): Gamestick
    {
        $gamestickRepo = new GamestickRepository();
        return $gamestickRepo->update($this->hwId, $values);
    }
}
