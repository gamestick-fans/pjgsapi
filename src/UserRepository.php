<?php
require_once 'User.php';
require_once 'Db.php';

/**
 * Functions to work with player profiles
 */
class UserRepository
{
    protected Db $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function findById(int $id): ?User
    {
        $stmt = $this->db->prepare('SELECT * FROM users WHERE id = :id');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch();
        return $row === false ? null : $row;
    }

    public function create(): User
    {
        $stmt = $this->db->prepare(
            <<<SQL
            INSERT INTO users
            (created_at, updated_at)
            VALUES (:created_at, :updated_at)
            SQL
        );
        $res = $stmt->execute(
            [
                ':created_at'       => date('Y-m-d H:i:s'),
                ':updated_at'       => date('Y-m-d H:i:s'),
            ]
        );
        $userId = $this->db->lastInsertId();
        return $this->findById($userId);
    }

    public function update(int $id, array $values): ?User
    {
        $params = [
            ':id'         => $id,
            ':updated_at' => date('Y-m-d H:i:s'),
        ];

        $sql = 'UPDATE users SET';
        $sqlParts = [
            ' updated_at = :updated_at'
        ];
        foreach ($values as $column => $value) {
            if (is_bool($value)) {
                //because we have int columns for true/false
                // and "" value is not allowed
                $value = intval($value);
            }
            $sqlParts[] = ' ' . $column . '= :' . $column;
            $params[':' . $column] = $value;
        }
        $sql .= implode(', ', $sqlParts) . ' WHERE id = :id';

        $stmt = $this->db->prepare($sql);
        $stmt->execute($params);

        return $this->findById($id);
    }
}
?>
