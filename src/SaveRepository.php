<?php
require_once 'Db.php';

class SaveRepository
{
    protected Db $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function createOrUpdate(int $userId, int $gameId, string $data): void
    {
        $stmt = $this->db->prepare(
            'SELECT id FROM saves'
            . ' WHERE userId = :userId AND gameId = :gameId'
        );
        $stmt->execute([':userId' => $userId, ':gameId' => $gameId]);
        $row = $stmt->fetchObject();

        if ($row === false) {
            $stmt = $this->db->prepare(
                <<<SQL
                INSERT INTO saves
                (userId, gameId, gameState, created_at, updated_at)
                VALUES (:userId, :gameId, :gameState, :created_at, :updated_at)
                SQL
            );
            $res = $stmt->execute(
                [
                    ':userId'           => $userId,
                    ':gameId'           => $gameId,
                    ':gameState'        => $data,
                    ':created_at'       => date('Y-m-d H:i:s'),
                    ':updated_at'       => date('Y-m-d H:i:s'),
                ]
            );

        } else {
            $stmt = $this->db->prepare(
                <<<SQL
                UPDATE saves SET
                    updated_at = :updated_at,
                    gameState  = :gameState
                WHERE id = :id
                SQL
            );
            $stmt->execute(
                [
                    ':id'          => $row->id,
                    ':gameState'   => $data,
                    ':updated_at'  => date('Y-m-d H:i:s'),
                ]
            );
        }
    }

    public function get(int $userId, int $gameId): ?string
    {
        $stmt = $this->db->prepare(
            'SELECT gameState FROM saves'
            . ' WHERE userId = :userId AND gameId = :gameId'
        );
        $stmt->execute([':userId' => $userId, ':gameId' => $gameId]);
        $row = $stmt->fetchObject();

        if ($row === false) {
            return null;
        }

        return $row->gameState;
    }
}
