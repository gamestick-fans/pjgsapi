<?php
require_once 'Gamestick.php';
require_once 'Db.php';

/**
 * Functions to work with gamestick hardware
 */
class GamestickRepository
{
    protected Db $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function findByHardwareId(string $hwId): ?Gamestick
    {
        $stmt = $this->db->prepare('SELECT * FROM gamesticks WHERE hwId = :id');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Gamestick');
        $stmt->execute([':id' => $hwId]);
        $row = $stmt->fetch();
        return $row === false ? null : $row;
    }

    public function findBySessionId(string $sessionId): ?Gamestick
    {
        $stmt = $this->db->prepare('SELECT * FROM gamesticks WHERE sessionId = :id');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Gamestick');
        $stmt->execute([':id' => $sessionId]);
        $row = $stmt->fetch();
        return $row === false ? null : $row;
    }

    public function findByVerificationCode(string $code): ?Gamestick
    {
        $stmt = $this->db->prepare('SELECT * FROM gamesticks WHERE verificationCode = :code');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Gamestick');
        $stmt->execute([':code' => $code]);
        $row = $stmt->fetch();
        return $row === false ? null : $row;
    }

    public function create(string $hwId): Gamestick
    {
        $stmt = $this->db->prepare(
            <<<SQL
            INSERT INTO gamesticks
            (hwId, sessionId, verificationCode, created_at, updated_at)
            VALUES (:hwId, :sessionId, :verificationCode, :created_at, :updated_at)
SQL
        );
        $stmt->execute(
            [
                ':hwId'             => $hwId,
                ':sessionId'        => 's' . str_replace(':', '', $hwId),
                ':verificationCode' => date('His'),
                ':created_at'       => date('Y-m-d H:i:s'),
                ':updated_at'       => date('Y-m-d H:i:s'),
            ]
        );
        return $this->findByHardwareId($hwId);
    }

    public function update(string $hwId, array $values): ?Gamestick
    {
        $params = [
            ':hwId'       => $hwId,
            ':updated_at' => date('Y-m-d H:i:s'),
        ];

        $sql = 'UPDATE gamesticks SET';
        $sqlParts = [
            ' updated_at = :updated_at'
        ];
        foreach ($values as $column => $value) {
            $sqlParts[] = ' ' . $column . '= :' . $column;
            $params[':' . $column] = $value;
        }
        $sql .= implode(', ', $sqlParts) . ' WHERE hwId = :hwId';

        $stmt = $this->db->prepare($sql);
        $stmt->execute($params);

        return $this->findByHardwareId($hwId);
    }
}
?>
