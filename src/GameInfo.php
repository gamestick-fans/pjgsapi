<?php

class GameInfo
{
    /**
     * Get the game id ("288") from a game version UUID
     * ("1bee7dfd135f991b5f00fbb68c71bee9")
     */
    public function getIdFromUuid(string $gameuuid): ?int
    {
        $cacheDir = __DIR__ . '/../cache/';
        $connectAppsCacheFile = $cacheDir . 'connect-apps.min.json';
        $data = json_decode(file_get_contents($connectAppsCacheFile));
        foreach ($data as $game) {
            if ($game->version == $gameuuid) {
                return $game->id;
            }
        }

        return null;
    }
}
