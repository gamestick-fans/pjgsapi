<?php
require_once 'Db.php';

class LeaderboardRepository
{
    protected Db $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function createOrUpdate(int $userId, int $gameId, int $score): void
    {
        $stmt = $this->db->prepare(
            'SELECT id, score FROM leaderboards'
            . ' WHERE userId = :userId AND gameId = :gameId'
        );
        $stmt->execute([':userId' => $userId, ':gameId' => $gameId]);
        $row = $stmt->fetchObject();

        if ($row === false) {
            $stmt = $this->db->prepare(
                <<<SQL
                INSERT INTO leaderboards
                (userId, gameId, score, created_at, updated_at)
                VALUES (:userId, :gameId, :score, :created_at, :updated_at)
                SQL
            );
            $res = $stmt->execute(
                [
                    ':userId'           => $userId,
                    ':gameId'           => $gameId,
                    ':score'            => $score,
                    ':created_at'       => date('Y-m-d H:i:s'),
                    ':updated_at'       => date('Y-m-d H:i:s'),
                ]
            );

        } else if ($score > $row->score) {
            $stmt = $this->db->prepare(
                <<<SQL
                UPDATE leaderboards SET
                    updated_at = :updated_at,
                    score = :score
                WHERE id = :id
                SQL
            );
            $stmt->execute(
                [
                    ':id'          => $row->id,
                    ':score'       => $score,
                    ':updated_at'  => date('Y-m-d H:i:s'),
                ]
            );
        }
    }

    /**
     * The best scores for a game
     *
     * @return array Keys: gamerTag, score
     */
    public function getTop50(int $gameId): array
    {
        $stmt = $this->db->prepare(
            <<<SQL
                SELECT gamerTag, score
                FROM leaderboards
                JOIN users
                  ON users.id = leaderboards.userId
                WHERE gameId = :gameId
                ORDER BY score DESC
                LIMIT 50
            SQL
        );
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $stmt->execute([':gameId' => $gameId]);

        return $stmt->fetchAll();
    }
}
