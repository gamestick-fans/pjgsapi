<?php

class Db extends PDO
{
    public function __construct()
    {
        parent::__construct(
            $GLOBALS['db']['dsn'],
            $GLOBALS['db']['username'], $GLOBALS['db']['password'],
            [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]
        );
    }
}
