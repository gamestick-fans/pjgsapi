<?php
require_once 'Gamestick.php';
require_once 'User.php';

/**
 * Check variables and send a HTTP failure and exit().
 */
class ApiResponses
{
    public static function checkAndGetSessionId(): string
    {
        if (!isset($_GET['jsessionid'])) {
            header('HTTP/1.0 400 Bad Request');
            header('Content-Type: text/plain');
            echo "Session ID missing\n";
            exit(1);
        }

        return $_GET['jsessionid'];
    }

    public static function checkGameId(?int $gameId)
    {
        if ($gameId === null) {
            header('HTTP/1.0 404 Not Found');
            header('Content-Type: text/plain');
            echo "Unknown game UUID\n";
            exit(1);
        }
    }

    public static function checkGamestick(?GameStick $gamestick)
    {
        if ($gamestick === null) {
            header('HTTP/1.0 404 Not Found');
            header('Content-Type: text/plain');
            echo "Unknown session ID\n";
            exit(1);
        }
    }

    public static function checkWhitelist(string $hwId)
    {
        if (!static::isWhitelisted($hwId)) {
            header('HTTP/1.0 403 Forbidden');
            header('Content-Type: text/plain');
            echo "Gamestick with this hardware ID may not use this server\n";
            exit(1);
        }
    }

    public static function isWhitelisted(string $hwId): bool
    {
        return !count($GLOBALS['whitelistedHardwareIds'])
            || array_search($hwId, $GLOBALS['whitelistedHardwareIds']) !== false;
    }

    public static function checkUserComplete(User $user)
    {
        if (!$user->complete()) {
            header('HTTP/1.0 403 Forbidden');
            header('Content-Type: text/plain');
            echo "Registration has not been completed yet\n";
            exit(1);
        }
    }
}
