PlayJam GameStick API server
****************************

Makes PlayJam GameSticks usable after the official server had been shut down
in 2017.


Setup
=====

1. Copy ``config.php.dist`` to ``config.php``
2. Clone the `game data repository <https://codeberg.org/gamestick-fans/game-data>`_::

     $ git clone https://codeberg.org/gamestick-fans/game-data.git
3. Unpack one of the cache files::

     $ gunzip -k game-data/caches/2017-01-12T00\:19\:12-alex.shorts.pretty.json.gz
4. Generate the popular and featured menus from the cache file,
   and extract the translation strings::

     $ ./bin/exract-all.sh 'game-data/caches/2017-01-12T00:19:12-alex.shorts.pretty.json'
5. Fill the caches for this API::

     $ ./bin/generate-apps-cache.php game-data/classic/
6. Setup the Apache web server and point the virtual host to the ``www/`` directory.


Firmware updates
================
Firmware update diff files are needed.
It is possible to send out full firmware files, but they
will reset the user data.

When you have such an update, put it into ``www/firmware-updates/$version/update.img``
and run ``./bin/prepare-firmware.sh www/firmware-updates/$version/``.

Also create a ``target-version.txt`` with the target version ("0.9.2071")
and a ``changelog.txt`` file.


About
=====
This server software was written by `Christian Weiske <https://cweiske.de/>`_
and is licensed under the AGPL v3.
