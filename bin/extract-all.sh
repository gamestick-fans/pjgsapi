#!/bin/sh
# extract all necessary files from a server connect cache file
set -e

if [ $# -lt 1 ]; then
    echo Error: connect cache json filename missing
    exit 1
fi

if [ ! -f "$1" ]; then
    echo Error: connect cache json file does not exist
    exit 2
fi

thisdir=$(dirname "$0")
cachedir=$thisdir/../cache

$thisdir/extract-uitranslation.sh "$1"\
    > "$cachedir/connect-uitranslation.min.json"

$thisdir/extract-popular.php "$1"\
    > "$cachedir/popular.txt"

$thisdir/extract-featured.php "$1"\
    > "$cachedir/featured.json"
