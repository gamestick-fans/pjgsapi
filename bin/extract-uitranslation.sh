#!/bin/sh
# extract the UI translation from a server cache file
set -e

if [ $# -lt 1 ]; then
    echo Error: connect cache json filename missing
    exit 1
fi

if [ ! -f "$1" ]; then
    echo Error: connect cache json file does not exist
    exit 2
fi

jq -c .body.config.global.uitranslation "$1"
