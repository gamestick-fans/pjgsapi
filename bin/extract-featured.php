#!/usr/bin/env php
<?php
/**
 * List all package names from a reg_server_response.json that are featured
 * in the menu.
 */
if ($argc <= 1) {
    fwrite(STDERR, "reg_server_response.json file missing\n");
    exit(1);
}

$regFile = $argv[1];
if (!file_exists($regFile)) {
    fwrite(STDERR, "json file does not exist: $regFile\n");
    exit(2);
}
if (!is_readable($regFile)) {
    fwrite(STDERR, "Cannot read json file: $regFile\n");
    exit(2);
}

$regData = json_decode(file_get_contents($regFile));
if ($regData === null) {
    fwrite(STDERR, "Cannot parse JSON data\n");
    fwrite(STDERR, json_last_error_msg() . "\n");
    exit(10);
}

if (!isset($regData->body->config->apps)) {
    fwrite(STDERR, "File contains no apps\n");
    exit(11);
}

$games = [];
foreach ($regData->body->config->apps as $app) {
    $games[$app->id] = $app->package;
}


$featured = [];
foreach ($regData->body->config->global->newfeatured->ages as $ageData) {
    $age = $ageData->age;
    foreach ($ageData->entries as $columnData) {
        foreach ($columnData->columnentries as $rowData) {
            if (!isset($games[$rowData->gameID])) {
                continue;
            }
            $package = $games[$rowData->gameID];
            $featured[$age][$package] = $rowData->thumbnail;
        }
    }
}

ksort($featured, SORT_NUMERIC);

echo json_encode(
    $featured,
    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
) . "\n";
