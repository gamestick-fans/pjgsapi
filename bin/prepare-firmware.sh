#!/bin/sh
# Prepare a firmware update file so it can be used for GameStick firmware updates
set -e

if ! command -v xortool-xor 2>&1 > /dev/null; then
    echo "Error: xortool-xor not found (pip install xortool)"
    exit 3
fi
if ! command -v xxd 2>&1 > /dev/null; then
    echo "Error: xxd not found"
    exit 3
fi

if [ $# -lt 1 ]; then
    echo "Error: Firmware directory missing (www/firmware-updates/x.y.z)"
    exit 1
fi

if [ ! -d "$1" ]; then
    echo "Error: Firmware directory does not exist"
    exit 2
fi

fwDir=$1
fwFile=$1/update.img

if [ ! -f "$fwFile" ]; then
    echo "Error: Firmware directory has no update.img file"
    exit 2
fi

changelogFile=$1/changelog.txt
if [ ! -f "$changelogFile" ]; then
    echo "Warning: Firmware directory has no changelog.txt file"
fi

targetFile=$1/target-version.txt
if [ ! -f "$targetFile" ]; then
    echo "Error: Firmware directory has no target-version.txt file"
    exit 2
fi

cd "$fwDir"
echo "Cleaning up"
rm chunk-* || true
rm tmp-* || true

echo "Splitting firmware update into chunks"
split --bytes=102400 --numeric-suffixes --suffix-length=4 $(basename "$fwFile") tmp-part-

#xor the files with the secret key
echo "Encrypting all chunks"
for partfile in tmp-part-*; do
    #"91" (hex 5b, binary 1011011) is the xor key
    xortool-xor -h 5b --no-newline -f "$partfile" > "tmp-xor-$partfile"
done

#"sign" the files by prefixing the binary sha1sum
echo "Signing all chunks"
for xorfile in tmp-xor-*; do
    #"91" (hex 5b, binary 1011011) is the xor key
    num=$(echo $xorfile | sed  's/tmp-xor-tmp-part-//')
    sha1sum "$xorfile" |cut -d' ' -f 1 | xxd -r -p > chunk-$num
    cat $xorfile >> chunk-$num
done

echo "Cleaning up temporary files"
rm tmp-* || true

#remove leading zeros
echo "Fixing filenames"
for num in $(seq 0 999); do
    if [ -f "chunk-0$num" ]; then
        mv "chunk-0$num" "chunk-$num"
    elif [ -f "chunk-00$num" ]; then
        mv "chunk-00$num" "chunk-$num"
    elif [ -f "chunk-000$num" ]; then
        mv "chunk-000$num" "chunk-$num"
    fi
done

#those files may not contain newlines
echo "Fixing chunk files"
ls -1 chunk-* | wc -l | tr -d '\n' > numchunks

echo "Generating file size file"
stat --printf=%s update.img > filesize
