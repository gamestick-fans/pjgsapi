#!/usr/bin/env php
<?php
/**
 * List all package names from a reg_server_response.json that are "popular"
 */
if ($argc <= 1) {
    fwrite(STDERR, "reg_server_response.json file missing\n");
    exit(1);
}

$regFile = $argv[1];
if (!file_exists($regFile)) {
    fwrite(STDERR, "json file does not exist: $regFile\n");
    exit(2);
}
if (!is_readable($regFile)) {
    fwrite(STDERR, "Cannot read json file: $regFile\n");
    exit(2);
}

$regData = json_decode(file_get_contents($regFile));
if ($regData === null) {
    fwrite(STDERR, "Cannot parse JSON data\n");
    fwrite(STDERR, json_last_error_msg() . "\n");
    exit(10);
}

if (!isset($regData->body->config->apps)) {
    fwrite(STDERR, "File contains no apps\n");
    exit(11);
}

$popular = [];
foreach ($regData->body->config->apps as $app) {
    if ($app->popular > 0) {
        $popular[$app->package] = $app->popular;
    }
}

asort($popular);
echo implode("\n", array_flip($popular)) . "\n";
